def fibonacci(n):
    sequence = []
    a, b = 0, 1
    for _ in range(n):
        sequence.append(a)
        a, b = b, a + b
    return sequence

# Número de termos da sequência que você deseja imprimir
num_terms = 100

fib_sequence = fibonacci(num_terms)
print(f"A sequência de Fibonacci com {num_terms} termos é:")
print(fib_sequence)
